---
platform: "Bitcoin Live"
date: 2020-06-02
publicationName: "Bitcoinlive with Leo Wandersleb on wallet security"
author: "@JDalmulder & @Cryptond on Twitter"
image: BitcoinLive.jpg
language: EN
format: video
transcript: false
link: https://www.youtube.com/watch?v=DvzOVIXxOj4
---
