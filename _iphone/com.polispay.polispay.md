---
wsId: PolisPay
title: "PolisPay - Crypto Wallet"
altTitle: 
authors:
- leo
appId: com.polispay.polispay
appCountry: 
idd: 1351572060
released: 2019-02-20
updated: 2021-03-24
version: "8.9.1"
score: 3.83333
reviews: 6
size: 34959360
developerWebsite: 
repository: 
issue: 
icon: com.polispay.polispay.jpg
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

