---
wsId: bitpaytrading
title: "BitBay - Bitcoin & Crypto"
altTitle: 
authors:
- leo
appId: com.bitbay.BitBayTrading
appCountry: 
idd: 1409644952
released: 2018-11-20
updated: 2021-03-30
version: "1.3.20"
score: 3.33333
reviews: 18
size: 99395584
developerWebsite: https://bitbay.net
repository: 
issue: 
icon: com.bitbay.BitBayTrading.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

