---
wsId: ZenGo
title: "ZenGo: Crypto & Bitcoin Wallet"
altTitle: 
authors:
- leo
appId: kzencorp.mobile.ios
appCountry: 
idd: 1440147115
released: 2019-06-07
updated: 2021-03-25
version: "2.22.1"
score: 4.60221
reviews: 1174
size: 70820864
developerWebsite: https://www.zengo.com
repository: 
issue: 
icon: kzencorp.mobile.ios.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

