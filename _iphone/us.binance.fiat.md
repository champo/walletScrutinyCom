---
wsId: BinanceUS
title: "Binance.US - Bitcoin & Crypto"
altTitle: 
authors:
- leo
appId: us.binance.fiat
appCountry: 
idd: 1492670702
released: 2020-01-05
updated: 2021-03-12
version: "2.3.4"
score: 4.24055
reviews: 29183
size: 118333440
developerWebsite: https://www.binance.us
repository: 
issue: 
icon: us.binance.fiat.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-01-10
reviewStale: true
signer: 
reviewArchive:


providerTwitter: binanceus
providerLinkedIn: company/binance-us
providerFacebook: BinanceUS
providerReddit: 

redirect_from:

---

This is the iPhone version of [this Android app](/android/com.binance.us) and we
come to the same conclusion for the same reasons. This app is **not verifiable**.
