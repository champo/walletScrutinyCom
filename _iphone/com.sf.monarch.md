---
wsId: Monarch
title: "Monarch Wallet"
altTitle: 
authors:
- leo
appId: com.sf.monarch
appCountry: 
idd: 1386397997
released: 2018-06-12
updated: 2021-03-11
version: "1.5.18"
score: 4.7972
reviews: 429
size: 145125376
developerWebsite: 
repository: 
issue: 
icon: com.sf.monarch.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

