---
wsId: sixdays
title: "Trust: Crypto & Bitcoin Wallet"
altTitle: 
authors:
- leo
appId: com.sixdays.trust
appCountry: 
idd: 1288339409
released: 2017-09-27
updated: 2021-03-24
version: "5.19"
score: 4.79599
reviews: 27748
size: 40178688
developerWebsite: https://trustwallet.com
repository: 
issue: 
icon: com.sixdays.trust.jpg
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-23
reviewStale: true
signer: 
reviewArchive:


providerTwitter: trustwalletapp
providerLinkedIn: 
providerFacebook: trustwalletapp
providerReddit: trustapp

redirect_from:

---

On the App Store they claim:

> Best digital wallet to securely store private keys on your device

While this is not an explicit claim of them not having a copy of those keys, it
is at least strongly implied.

On their website they are more specific:

> **Private & Secure**<br>
  Only you can access your wallet. We don’t collect any personal data.

but as with [their Android app](/android/com.wallet.crypto.trustapp/) there is
no public source of the iPhone app neither.

Without public code the verdict is: **not verifiable**.
