---
wsId: Sylo
title: "Sylo"
altTitle: 
authors:
- leo
appId: io.sylo.dapp
appCountry: 
idd: 1452964749
released: 2019-09-10
updated: 2021-03-24
version: "3.0.15"
score: 4.72222
reviews: 18
size: 179731456
developerWebsite: https://www.sylo.io/wallet/
repository: 
issue: 
icon: io.sylo.dapp.jpg
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

