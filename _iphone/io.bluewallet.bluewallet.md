---
wsId: bluewallet
title: "BlueWallet - Bitcoin wallet"
altTitle: 
authors:
- leo
appId: io.bluewallet.bluewallet
appCountry: 
idd: 1376878040
released: 2018-05-27
updated: 2021-03-25
version: "6.0.7"
score: 4.20791
reviews: 202
size: 66172928
developerWebsite: 
repository: 
issue: 
icon: io.bluewallet.bluewallet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

