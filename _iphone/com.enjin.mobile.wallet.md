---
wsId: 
title: "Enjin Crypto Blockchain Wallet"
altTitle: 
authors:
- leo
appId: com.enjin.mobile.wallet
appCountry: 
idd: 1349078375
released: 2018-03-12
updated: 2021-03-25
version: "1.13.0"
score: 4.57233
reviews: 470
size: 43458560
developerWebsite: https://enjin.io/products/wallet
repository: 
issue: 
icon: com.enjin.mobile.wallet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

