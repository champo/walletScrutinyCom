---
wsId: bitpie
title: "Bitpie-Universal Crypto Wallet"
altTitle: 
authors:
- leo
appId: com.bitpie.wallet
appCountry: 
idd: 1481314229
released: 2019-10-01
updated: 2021-03-17
version: "5.0.018"
score: 3.19048
reviews: 21
size: 276353024
developerWebsite: 
repository: 
issue: 
icon: com.bitpie.wallet.jpg
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

