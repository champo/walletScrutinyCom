---
wsId: SwipeWallet
title: "Swipe Wallet"
altTitle: 
authors:
- leo
appId: com.swipe.wallet
appCountry: 
idd: 1476726454
released: 2019-09-10
updated: 2021-03-22
version: "1.532"
score: 4.72881
reviews: 1180
size: 142386176
developerWebsite: https://swipe.io
repository: 
issue: 
icon: com.swipe.wallet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

