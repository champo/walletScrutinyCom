---
wsId: krakent
title: "Kraken Pro"
altTitle: 
authors:
- leo
appId: com.kraken.trade.app
appCountry: 
idd: 1473024338
released: 2019-11-12
updated: 2021-03-25
version: "1.5.13"
score: 4.62535
reviews: 5696
size: 33909760
developerWebsite: 
repository: 
issue: 
icon: com.kraken.trade.app.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

