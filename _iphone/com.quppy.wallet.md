---
wsId: Quppy
title: "Quppy – Secure Bitcoin Walle‪t"
altTitle: 
authors:
- leo
appId: com.quppy.wallet
appCountry: 
idd: 1417802076
released: 2018-08-09
updated: 2021-03-17
version: "1.0.46"
score: 4.86111
reviews: 144
size: 46093312
developerWebsite: https://quppy.com
repository: 
issue: 
icon: com.quppy.wallet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

