---
wsId: ownbit
title: "Ownbit: Cold & MultiSig Wallet"
altTitle: 
authors:
- leo
appId: com.bitbill.wallet
appCountry: 
idd: 1321798216
released: 2018-02-07
updated: 2021-03-28
version: "4.28.1"
score: 4.46
reviews: 50
size: 115326976
developerWebsite: 
repository: 
issue: 
icon: com.bitbill.wallet.jpg
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

