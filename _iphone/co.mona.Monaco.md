---
wsId: mona
title: "Crypto.com - Buy Bitcoin Now"
altTitle: 
authors:
- leo
appId: co.mona.Monaco
appCountry: 
idd: 1262148500
released: 2017-08-31
updated: 2021-03-23
version: "3.88"
score: 4.38448
reviews: 14128
size: 262732800
developerWebsite: https://crypto.com/
repository: 
issue: 
icon: co.mona.Monaco.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-01-11
reviewStale: true
signer: 
reviewArchive:


providerTwitter: cryptocom
providerLinkedIn: company/cryptocom
providerFacebook: CryptoComOfficial
providerReddit: Crypto_com

redirect_from:

---

As their [version for Android](/android/co.mona.android) this app is custodial
and thus **not verifiable**.
