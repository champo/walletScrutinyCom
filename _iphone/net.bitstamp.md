---
wsId: Bitstamp
title: "Bitstamp – crypto exchange app"
altTitle: 
authors:
- leo
appId: net.bitstamp
appCountry: 
idd: 1406825640
released: 2019-01-30
updated: 2021-03-25
version: "2.0"
score: 4.81806
reviews: 4430
size: 86329344
developerWebsite: https://www.bitstamp.net/
repository: 
issue: 
icon: net.bitstamp.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-01-23
reviewStale: true
signer: 
reviewArchive:


providerTwitter: Bitstamp
providerLinkedIn: company/bitstamp
providerFacebook: Bitstamp
providerReddit: 

redirect_from:

---

[Just like on Google Play](/android/net.bitstamp.app), they claim:

> Convenient, but secure<br>
  ● We store 98% of all crypto assets in cold storage

which means you don't get the keys for your coins. This is a custodial service
and therefore **not verifiable**.
