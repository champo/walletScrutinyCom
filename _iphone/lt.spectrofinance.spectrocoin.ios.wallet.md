---
wsId: SpectroCoin
title: "Bitcoin Wallet by SpectroCoin"
altTitle: 
authors:
- leo
appId: lt.spectrofinance.spectrocoin.ios.wallet
appCountry: 
idd: 923696089
released: 2014-12-30
updated: 2020-12-18
version: "1.16.0"
score: 2.90909
reviews: 11
size: 52452352
developerWebsite: https://spectrocoin.com/
repository: 
issue: 
icon: lt.spectrofinance.spectrocoin.ios.wallet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

