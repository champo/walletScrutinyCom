---
wsId: Unstoppable
title: "Unstoppable Wallet"
altTitle: 
authors:
- leo
appId: io.horizontalsystems.bank-wallet
appCountry: 
idd: 1447619907
released: 2019-01-10
updated: 2021-02-17
version: "0.19.1"
score: 4.68354
reviews: 237
size: 43611136
developerWebsite: https://unstoppable.money/
repository: https://github.com/horizontalsystems/unstoppable-wallet-ios
issue: 
icon: io.horizontalsystems.bank-wallet.jpg
bugbounty: 
verdict: nonverifiable # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-19
reviewStale: true
signer: 
reviewArchive:


providerTwitter: unstoppablebyhs
providerLinkedIn: 
providerFacebook: 
providerReddit: UNSTOPPABLEWallet

redirect_from:

---

The provider claims:

> A non-custodial wallet without third party risk.

and we found the source code
[here](https://github.com/horizontalsystems/unstoppable-wallet-ios)
but so far nobody reproduced the build, so the claim is **not verifiable**.
