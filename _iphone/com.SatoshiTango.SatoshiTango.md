---
wsId: SatoshiTango
title: "SatoshiTango"
altTitle: 
authors:
- leo
appId: com.SatoshiTango.SatoshiTango
appCountry: 
idd: 1002555958
released: 2015-07-08
updated: 2021-02-19
version: "3.4.17"
score: 4.32258
reviews: 31
size: 133840896
developerWebsite: http://www.satoshitango.com
repository: 
issue: 
icon: com.SatoshiTango.SatoshiTango.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

