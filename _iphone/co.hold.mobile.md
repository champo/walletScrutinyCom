---
wsId: coholdmobile
title: "HOLD — Buy Bitcoin & Crypto"
altTitle: 
authors:
- leo
appId: co.hold.mobile
appCountry: de
idd: 1435187229
released: 2018-09-28
updated: 2021-03-09
version: "3.12.9"
score: 3.93333
reviews: 15
size: 33201152
developerWebsite: https://hold.io
repository: 
issue: 
icon: co.hold.mobile.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-03-11
reviewStale: true
signer: 
reviewArchive:


providerTwitter: HoldHQ
providerLinkedIn: company/holdhq
providerFacebook: HoldHQ
providerReddit: 

redirect_from:

---

> SAFETY FIRST<br>
  Regulated and licensed in the EU. Your money is securely held by banks within
  the European Union and your crypto protected by the world-renowned custodian
  BitGo.

This app is a custodial offering and thus **not verifiable**.

