---
wsId: ChangeNOW
title: "ChangeNOW Crypto Exchange"
altTitle: 
authors:
- leo
appId: io.changenow
appCountry: 
idd: 1518003605
released: 2020-06-29
updated: 2021-03-15
version: "1.4.2"
score: 4.43919
reviews: 148
size: 29977600
developerWebsite: 
repository: 
issue: 
icon: io.changenow.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

