---
wsId: coinomi
title: "Coinomi Wallet"
altTitle: 
authors:
- leo
appId: com.coinomi.wallet
appCountry: 
idd: 1333588809
released: 2018-03-22
updated: 2021-02-11
version: "1.9.3"
score: 4.49554
reviews: 896
size: 115743744
developerWebsite: https://www.coinomi.com
repository: 
issue: 
icon: com.coinomi.wallet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

