---
wsId: Vidulum
title: "Vidulum"
altTitle: 
authors:
- leo
appId: com.vidulum.app
appCountry: 
idd: 1505859171
released: 2020-07-28
updated: 2021-03-26
version: "1.1.9"
score: 5
reviews: 5
size: 60642304
developerWebsite: https://vidulum.app
repository: 
issue: 
icon: com.vidulum.app.jpg
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

