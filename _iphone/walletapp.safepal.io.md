---
wsId: 
title: "SafePal Wallet"
altTitle: 
authors:
- leo
appId: walletapp.safepal.io
appCountry: 
idd: 1548297139
released: 2021-01-11
updated: 2021-02-25
version: "2.5.7"
score: 3.96491
reviews: 57
size: 113358848
developerWebsite: https://www.safepal.io/
repository: 
issue: 
icon: walletapp.safepal.io.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-01-16
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

