---
wsId: CashApp
title: "Cash App"
altTitle: 
authors:
- leo
appId: com.squareup.cash
appCountry: 
idd: 711923939
released: 2013-10-16
updated: 2021-03-16
version: "3.35"
score: 4.75319
reviews: 1846328
size: 225560576
developerWebsite: https://cash.app
repository: 
issue: 
icon: com.squareup.cash.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: cashapp
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

On their website the provider claims:

> **Coin Storage**<br>
  Your Bitcoin balance is securely stored in our offline system

which means it is custodial.
