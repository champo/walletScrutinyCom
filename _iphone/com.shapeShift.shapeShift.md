---
wsId: ShapeShift
title: "ShapeShift: Buy & Trade Crypto"
altTitle: 
authors:
- leo
appId: com.shapeShift.shapeShift
appCountry: 
idd: 996569075
released: 2015-06-09
updated: 2021-03-20
version: "2.13.0"
score: 2.93516
reviews: 401
size: 77983744
developerWebsite: https://shapeshift.com
repository: 
issue: 
icon: com.shapeShift.shapeShift.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

