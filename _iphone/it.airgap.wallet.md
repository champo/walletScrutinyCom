---
wsId: AirGapWallet
title: "AirGap Wallet"
altTitle: 
authors:

appId: it.airgap.wallet
appCountry: 
idd: 1420996542
released: 2018-08-24
updated: 2021-03-01
version: "3.6.3"
score: 3.66667
reviews: 3
size: 40666112
developerWebsite: 
repository: 
issue: 
icon: it.airgap.wallet.jpg
bugbounty: 
verdict: nowallet # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-03-07
reviewStale: true
signer: 
reviewArchive:


providerTwitter: AirGap_it
providerLinkedIn: 
providerFacebook: 
providerReddit: AirGap

redirect_from:

---

This is the iPhone version of [this Android app](/android/it.airgap.wallet/).
