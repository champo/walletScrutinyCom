---
wsId: YouHodler
title: "YouHodler - Bitcoin Wallet"
altTitle: 
authors:
- leo
appId: com.youhodler.youhodler
appCountry: 
idd: 1469351696
released: 2019-07-25
updated: 2021-03-15
version: "2.11.0"
score: 4.81152
reviews: 191
size: 35323904
developerWebsite: https://www.youhodler.com/
repository: 
issue: 
icon: com.youhodler.youhodler.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

