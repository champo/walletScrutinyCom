---
wsId: xapo
title: "Xapo"
altTitle: 
authors:
- leo
appId: com.iphone.XapoApp
appCountry: 
idd: 917692892
released: 2014-11-13
updated: 2020-08-21
version: "6.11.1"
score: 3.96429
reviews: 140
size: 228537344
developerWebsite: https://xapo.com
repository: 
issue: 
icon: com.iphone.XapoApp.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

