---
wsId: casaapp
title: "Casa App - Secure your Bitcoin"
altTitle: 
authors:
- leo
appId: com.casa.vault
appCountry: 
idd: 1314586706
released: 2018-08-02
updated: 2021-03-18
version: "2.33"
score: 4.91633
reviews: 251
size: 42533888
developerWebsite: https://keys.casa
repository: 
issue: 
icon: com.casa.vault.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

