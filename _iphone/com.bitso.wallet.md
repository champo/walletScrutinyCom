---
wsId: bitso
title: "Bitso - Buy and sell bitcoin"
altTitle: 
authors:
- leo
appId: com.bitso.wallet
appCountry: 
idd: 1292836438
released: 2018-02-19
updated: 2021-03-16
version: "2.17.0"
score: 3.57143
reviews: 42
size: 90011648
developerWebsite: https://bitso.com/app
repository: 
issue: 
icon: com.bitso.wallet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

