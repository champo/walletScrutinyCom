---
wsId: XcelPay
title: "XcelPay - Secure Crypto Wallet"
altTitle: 
authors:
- leo
appId: com.xcelpay.wallet
appCountry: 
idd: 1461215417
released: 2019-05-26
updated: 2021-03-17
version: "2.8.15"
score: 4.05263
reviews: 19
size: 38971392
developerWebsite: http://xcelpay.io
repository: 
issue: 
icon: com.xcelpay.wallet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

