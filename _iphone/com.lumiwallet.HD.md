---
wsId: LumiWallet
title: "Bitcoin Wallet by Lumi Wallet"
altTitle: 
authors:
- leo
appId: com.lumiwallet.HD
appCountry: 
idd: 1316477906
released: 2017-12-08
updated: 2021-03-30
version: "3.10.0"
score: 4.81742
reviews: 2996
size: 79796224
developerWebsite: https://lumiwallet.com/
repository: 
issue: 
icon: com.lumiwallet.HD.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

