---
wsId: cake
title: "Cake Wallet"
altTitle: 
authors:
- leo
appId: com.fotolockr.cakewallet
appCountry: 
idd: 1334702542
released: 2018-01-19
updated: 2021-03-24
version: "4.1.3"
score: 3.64467
reviews: 197
size: 132476928
developerWebsite: http://cakewallet.com
repository: https://github.com/cake-tech/cake_wallet
issue: 
icon: com.fotolockr.cakewallet.jpg
bugbounty: 
verdict: nonverifiable # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-02-07
reviewStale: true
signer: 
reviewArchive:


providerTwitter: cakewallet
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

> Cake Wallet allows you to safely store, send receive and exchange your XMR /
  Monero and BTC / Bitcoin.

is an implicit claim of this being a non-custodial Bitcoin wallet but:

> -You control your own seed and keys

is more explicit about the non-custodial part.

On their website we read:

> **FEATURES**<br>
  ...<br>
  Open source

and indeed, there is [a source code repo](https://github.com/cake-tech/cake_wallet).

There is no claim about reproducibility or build instructions. As the app uses
[Flutter](https://flutter.dev/) and we have no experience with that, we have to
stop here. Usually at this point we open issues on the code repository but they
have no public issue tracker.
