---
wsId: stormgain
title: "StormGain: Bitcoin Wallet & Crypto Exchange App"
altTitle: 
authors:
- leo
users: 1000000
appId: com.stormgain.mobile
launchDate: 
latestUpdate: 2021-02-26
apkVersionName: "1.14.1"
stars: 4.5
ratings: 16972
reviews: 10111
size: 23M
website: https://stormgain.com
repository: 
issue: 
icon: com.stormgain.mobile.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-03-10
reviewStale: true
signer: 
reviewArchive:


providerTwitter: StormGain_com
providerLinkedIn: 
providerFacebook: StormGain.official
providerReddit: 

redirect_from:

---


This app's description mainly focuses on trading and interest earning, features
usually associated with custodial offerings. Also their little paragraph on
security:

> With Industry-Leading Security Protocols, Two-Factor Authentication and Cold
  Wallet Storage, StormGain is a secure environment for Crypto Trading.

sounds more like a custodial offering with the "Cold Wallet Storage" in there.
We conclude the app is **not verifiable**.
