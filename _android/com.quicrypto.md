---
wsId: 
title: "Quicrypto: Earn Crypto & Free Bitcoin"
altTitle: 
authors:

users: 100000
appId: com.quicrypto
launchDate: 
latestUpdate: 2021-03-03
apkVersionName: "3.2.0"
stars: 3.8
ratings: 4441
reviews: 2736
size: 26M
website: http://www.quicrypto.com
repository: 
issue: 
icon: com.quicrypto.png
bugbounty: 
verdict: nowallet # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-01
reviewStale: true
signer: 
reviewArchive:


providerTwitter: quicrypto
providerLinkedIn: 
providerFacebook: Quicrypto
providerReddit: 

redirect_from:
  - /com.quicrypto/
---


This app "pays" you in Bitcoin and Nano for tasks

> What is Quicrypto?
> 
> Quicrypto is a “reward app” that allows its users to earn money (in the form of the Nano cryptocurrency) by watching ads and completeing offers.

and one of the early tasks appears to be to give a five star rating:

> Filip Zdravkovic
> 
> What a scam.Unfortunatelly people still fall for it and the only reason it has
  a good rating is because they promise you a some coins for it.Do not download
  this useless virus app.

They also have instructions on how to get a wallet, so ... it's not a wallet
itself.
