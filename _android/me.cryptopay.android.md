---
wsId: cryptopay
title: "C.PAY"
altTitle: 
authors:

users: 50000
appId: me.cryptopay.android
launchDate: 
latestUpdate: 2021-03-09
apkVersionName: "1.23.1"
stars: 4.3
ratings: 591
reviews: 304
size: 16M
website: https://cryptopay.me
repository: 
issue: 
icon: me.cryptopay.android.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-03-10
reviewStale: true
signer: 
reviewArchive:


providerTwitter: cryptopay
providerLinkedIn: company/cryptopay
providerFacebook: cryptopayme
providerReddit: 

redirect_from:

---


In the description the only sentence hinting at custodianship is:

> Use our secure multisig wallet to receive, store and transfer BTC, LTC, XRP,
  ETH to your friends.

but there is nothing more to be found and as "multisig wallet" could refer to
anything, we can't say with certainty that this wallet even tries to imply
being self-custodial and therefore consider it **not verifiable**.
