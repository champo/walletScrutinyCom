---
wsId: 
title: "Bitcoin of America Wallet"
altTitle: 
authors:
- leo
users: 10000
appId: com.bitcoinofamerica.wallet
launchDate: 
latestUpdate: 2020-04-01
apkVersionName: "1.1.2"
stars: 4.3
ratings: 153
reviews: 47
size: 55M
website: https://www.bitcoinofamerica.org
repository: 
issue: 
icon: com.bitcoinofamerica.wallet.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-03-08
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


In the description we read:

> Bitcoin of America Wallet is a powerful and easy to use cryptocurrency wallet
  that allows users to easily control their own private keys with the
  familiarity and ease of mobile banking.

and

> Open-source code. Available at `https://github.com/Airbitz`

but [that GitHub account](https://github.com/Airbitz) is of
[another wallet](/android/com.airbitz/) and certainly not under the provided
link as there is no code there
[or anywhere else on GitLab](https://github.com/search?q=com.bitcoinofamerica.wallet).

As the provider's website also is down as of now, we don't expect much from this
wallet and file it as **not verifiable**.
