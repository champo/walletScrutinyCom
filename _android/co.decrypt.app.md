---
wsId: 
title: "Decrypt - Bitcoin & crypto news"
altTitle: 
authors:

users: 50000
appId: co.decrypt.app
launchDate: 
latestUpdate: 2021-03-16
apkVersionName: "2.1.2"
stars: 4.2
ratings: 871
reviews: 435
size: 27M
website: 
repository: 
issue: 
icon: co.decrypt.app.png
bugbounty: 
verdict: nowallet # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-08
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /co.decrypt.app/
---


This app only provides news about Bitcoin but no wallet itself.
