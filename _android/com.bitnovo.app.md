---
wsId: bitnovo
title: "Bitnovo - Crypto Wallet"
altTitle: 
authors:
- leo
users: 10000
appId: com.bitnovo.app
launchDate: 
latestUpdate: 2021-02-23
apkVersionName: "2.8.4"
stars: 2.5
ratings: 264
reviews: 189
size: 34M
website: http://www.bitnovo.com
repository: 
issue: 
icon: com.bitnovo.app.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-06
reviewStale: true
signer: 
reviewArchive:


providerTwitter: bitnovo
providerLinkedIn: 
providerFacebook: BitcoinBitnovo
providerReddit: 

redirect_from:
  - /com.bitnovo.app/
---


The reviews don't let us expect this wallet to be around for long. 2.7* with
tons of scam accusations.

The provider clearly controls the funds and thus this "wallet" is **not
verifiable**.

