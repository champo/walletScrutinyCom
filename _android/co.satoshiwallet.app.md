---
wsId: 
title: "SatoshiWallet for Bitcoin, Ethereum, Monero & more"
altTitle: 
authors:

users: 1000
appId: co.satoshiwallet.app
launchDate: 
latestUpdate: 2020-01-17
apkVersionName: "2.1.3"
stars: 3.5
ratings: 14
reviews: 11
size: Varies with device
website: 
repository: 
issue: 
icon: co.satoshiwallet.app.png
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-03-05
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


<!-- https://gitlab.com/walletscrutiny/walletScrutinyCom/-/issues/171 -->
