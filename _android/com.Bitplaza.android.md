---
wsId: bitplaza
title: "Bitplaza - Shopping With Bitcoin"
altTitle: 
authors:

users: 10000
appId: com.Bitplaza.android
launchDate: 
latestUpdate: 2018-11-14
apkVersionName: "4.0"
stars: 4.5
ratings: 105
reviews: 51
size: 1.7M
website: https://www.bitplazashopping.com
repository: 
issue: 
icon: com.Bitplaza.android.png
bugbounty: 
verdict: nowallet # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-03-10
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This app is a market place with no integrated wallet.
