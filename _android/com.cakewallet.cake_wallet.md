---
wsId: cake
title: "Cake Wallet"
altTitle: 
authors:
- leo
users: 10000
appId: com.cakewallet.cake_wallet
launchDate: 
latestUpdate: 2021-03-24
apkVersionName: "4.1.3"
stars: 4.1
ratings: 365
reviews: 231
size: 62M
website: https://cakewallet.com
repository: https://github.com/cake-tech/cake_wallet
issue: 
icon: com.cakewallet.cake_wallet.png
bugbounty: 
verdict: nonverifiable # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-02-07
reviewStale: true
signer: 
reviewArchive:


providerTwitter: cakewallet
providerLinkedIn: 
providerFacebook: cakewallet
providerReddit: cakewallet

redirect_from:

---


> Cake Wallet allows you to safely store, send receive and exchange your XMR /
  Monero and BTC / Bitcoin.

is an implicit claim of this being a non-custodial Bitcoin wallet but:

> -You control your own seed and keys

is more explicit about the non-custodial part.

On their website we read:

> **FEATURES**<br>
  ...<br>
  Open source

and indeed, there is [a source code repo](https://github.com/cake-tech/cake_wallet).

There is no claim about reproducibility or build instructions. As the app uses
[Flutter](https://flutter.dev/) and we have no experience with that, we have to
stop here. Usually at this point we open issues on the code repository but they
have no public issue tracker.
