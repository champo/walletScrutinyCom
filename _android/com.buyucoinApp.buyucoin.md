---
wsId: buyucoin
title: "BuyUcoin : Crypto Wallet to Buy/Sell Bitcoin India"
altTitle: 
authors:
- leo
users: 10000
appId: com.buyucoinApp.buyucoin
launchDate: 
latestUpdate: 2021-03-26
apkVersionName: "3.6"
stars: 3.6
ratings: 490
reviews: 346
size: 17M
website: https://www.buyucoin.com
repository: 
issue: 
icon: com.buyucoinApp.buyucoin.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-06-20
reviewStale: true
signer: 
reviewArchive:


providerTwitter: buyucoin
providerLinkedIn: company/buyucoin
providerFacebook: BuyUcoin
providerReddit: 

redirect_from:
  - /com.buyucoinApp.buyucoin/
  - /posts/com.buyucoinApp.buyucoin/
---


This app appears to be the broken interface for a broken exchange, judging by
the vast majority of user comments. It is certainly **not verifiable**.
