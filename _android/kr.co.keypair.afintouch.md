---
wsId: krcokeypair
title: "AFIN Touch"
altTitle: 
authors:

users: 100
appId: kr.co.keypair.afintouch
launchDate: 
latestUpdate: 2019-12-27
apkVersionName: "1.0.0.60"
stars: 0.0
ratings: 
reviews: 
size: 11M
website: https://www.afincoin.io
repository: 
issue: 
icon: kr.co.keypair.afintouch.png
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-03-07
reviewStale: true
signer: 
reviewArchive:


providerTwitter: AfinCoin
providerLinkedIn: 
providerFacebook: asianfintech
providerReddit: 

redirect_from:

---


<!-- nosource -->
As far as we can see, this is the same as
[this app](/android/kr.co.keypair.keywalletTouch) and thus is **not verifiable**.

