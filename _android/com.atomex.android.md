---
wsId: 
title: "Atomex - Crypto Wallet & Atomic swap DEX"
altTitle: 
authors:

users: 1000
appId: com.atomex.android
launchDate: 
latestUpdate: 2021-03-25
apkVersionName: "1.7"
stars: 4.8
ratings: 31
reviews: 30
size: 59M
website: 
repository: 
issue: 
icon: com.atomex.android.png
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-03-05
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


<!-- https://gitlab.com/walletscrutiny/walletScrutinyCom/-/issues/177 -->
