---
wsId: 
title: "Changelly: Buy Bitcoin BTC & Fast Crypto Exchange"
altTitle: 
authors:

users: 100000
appId: com.changelly.app
launchDate: 
latestUpdate: 2021-03-01
apkVersionName: "2.7.2.1"
stars: 4.3
ratings: 1924
reviews: 1147
size: 6.7M
website: 
repository: 
issue: 
icon: com.changelly.app.png
bugbounty: 
verdict: nowallet # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.changelly.app/
  - /posts/com.changelly.app/
---


This app has no wallet feature in the sense that you hold Bitcoins in the app.
