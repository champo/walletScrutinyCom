---
wsId: 
title: "Electroneum"
altTitle: 
authors:

users: 1000000
appId: com.electroneum.mobile
launchDate: 
latestUpdate: 2021-02-16
apkVersionName: "5.0.0"
stars: 2.5
ratings: 57411
reviews: 36964
size: 18M
website: 
repository: 
issue: 
icon: com.electroneum.mobile.png
bugbounty: 
verdict: nobtc # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-06-08
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.electroneum.mobile/
  - /posts/com.electroneum.mobile/
---


This app does not support storing BTC.

*(Besides that, we couldn't find any source code or even a claim of it being
non-custodial.)*
