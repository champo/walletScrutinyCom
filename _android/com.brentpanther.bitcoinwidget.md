---
wsId: 
title: "Simple Bitcoin Widget"
altTitle: 
authors:

users: 100000
appId: com.brentpanther.bitcoinwidget
launchDate: 
latestUpdate: 2021-02-18
apkVersionName: "7.4"
stars: 3.9
ratings: 1749
reviews: 687
size: 3.4M
website: 
repository: 
issue: 
icon: com.brentpanther.bitcoinwidget.png
bugbounty: 
verdict: nowallet # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-03-05
reviewStale: false
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


> Note: This is only a widget. You must add the widget to your launcher, it will
  not appear in your apps list.
