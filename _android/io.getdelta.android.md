---
wsId: 
title: "Delta Investment Portfolio Tracker"
altTitle: 
authors:

users: 500000
appId: io.getdelta.android
launchDate: 
latestUpdate: 2021-03-05
apkVersionName: "4.0.7"
stars: 3.8
ratings: 18467
reviews: 7245
size: 39M
website: 
repository: 
issue: 
icon: io.getdelta.android.png
bugbounty: 
verdict: nowallet # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-01
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /io.getdelta.android/
---


This appears to be only a portfolio tracker. If it asks for your credentials for
exchanges, it might still get into a position of pulling your funds from there.
