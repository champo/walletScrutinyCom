---
wsId: 
title: "ZEBEDEE Wallet"
altTitle: 
authors:
- leo
users: 1000
appId: io.zebedee.wallet
launchDate: 
latestUpdate: 2021-03-10
apkVersionName: "11.0.0"
stars: 4.7
ratings: 15
reviews: 9
size: 59M
website: 
repository: 
issue: 
icon: io.zebedee.wallet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-11-28
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /io.zebedee.wallet/
---


