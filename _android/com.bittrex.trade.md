---
wsId: bittrex
title: "Bittrex Global"
altTitle: 
authors:
- leo
users: 100000
appId: com.bittrex.trade
launchDate: 
latestUpdate: 2021-03-12
apkVersionName: "1.13.2"
stars: 2.4
ratings: 1206
reviews: 793
size: 49M
website: https://global.bittrex.com
repository: 
issue: 
icon: com.bittrex.trade.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-02-09
reviewStale: true
signer: 
reviewArchive:


providerTwitter: BittrexGlobal
providerLinkedIn: 
providerFacebook: BittrexGlobal
providerReddit: 

redirect_from:

---


This app is an interface to a trading platform:

> The Bittrex Global mobile app allows you to take the premiere crypto trading
  platform with you wherever you go.

As such, it lets you access your account with them but not custody your own
coins and therefore is **not verifiable**.
