---
wsId: 
title: "Smart Coin Wallet For Android"
altTitle: 
authors:

users: 10000
appId: io.smcc.sccwallet
launchDate: 
latestUpdate: 2018-10-02
apkVersionName: "1.29"
stars: 4.3
ratings: 9
reviews: 2
size: 4.1M
website: 
repository: 
issue: 
icon: io.smcc.sccwallet.png
bugbounty: 
verdict: nobtc # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /io.smcc.sccwallet/
  - /posts/io.smcc.sccwallet/
---


This is not a BTC wallet. It appears to only support some smart coin.
