---
wsId: 
title: "MAX-Wallet"
altTitle: 
authors:
- leo

users: 5000
appId: com.maxonrow.wallet
launchDate: 2019-08-31
latestUpdate: 2020-10-20
apkVersionName: "2.1.16"
stars: 3.9
ratings: 73
reviews: 43
size: 55M
website: https://www.maxonrow.com
repository: 
issue: 
icon: com.maxonrow.wallet.png
bugbounty: 
verdict: defunct # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-03-17
reviewStale: false
signer: 
reviewArchive:
- date: 2019-12-30
  version: 
  apkHash: 
  gitRevision: b75c3bd9d94b20365881eb5ec32b299d3c317f87
  verdict: nobtc


providerTwitter: maxonrow
providerLinkedIn: company/maxonrow
providerFacebook: maxonrowblockchain
providerReddit: 

redirect_from:
---


We find no mention of Bitcoin on the description or the website.
