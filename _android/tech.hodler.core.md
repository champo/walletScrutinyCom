---
wsId: 
title: "HODLER Open Source Multi-Asset Wallet"
altTitle: 
authors:

users: 1000
appId: tech.hodler.core
launchDate: 
latestUpdate: 2019-10-29
apkVersionName: "0.4.6"
stars: 4.0
ratings: 47
reviews: 35
size: 19M
website: 
repository: 
issue: 
icon: tech.hodler.core.png
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-03-08
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


<!-- https://gitlab.com/walletscrutiny/walletScrutinyCom/-/issues/190 -->
