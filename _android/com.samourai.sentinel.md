---
wsId: 
title: "Sentinel"
altTitle: 
authors:

users: 10000
appId: com.samourai.sentinel
launchDate: 
latestUpdate: 2020-09-04
apkVersionName: "4.0.1"
stars: 4.0
ratings: 273
reviews: 143
size: 40M
website: https://www.samouraiwallet.com/sentinel
repository: https://github.com/Samourai-Wallet/sentinel-android
issue: 
icon: com.samourai.sentinel.png
bugbounty: 
verdict: nowallet # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-04-07
reviewStale: true
signer: 
reviewArchive:


providerTwitter: samouraiwallet
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.samourai.sentinel/
  - /posts/com.samourai.sentinel/
---


As this app has no private keys to protect, we do not consider it a wallet.
