---
wsId: bit2me
title: "Bit2Me - Buy and Sell Cryptocurrencies"
altTitle: 
authors:
- leo
users: 50000
appId: com.phonegap.bit2me
launchDate: 
latestUpdate: 2021-02-21
apkVersionName: "2.0.52"
stars: 4.4
ratings: 1210
reviews: 593
size: 17M
website: https://bit2me.com
repository: 
issue: 
icon: com.phonegap.bit2me.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: bit2me
providerLinkedIn: company/bit2me
providerFacebook: bit2me
providerReddit: 

redirect_from:
  - /com.phonegap.bit2me/
  - /posts/com.phonegap.bit2me/
---


This appears to be the interface for an exchange. We could not find any claims
about you owning your keys. As a custodial service it is **not verifiable**.
