---
wsId: 
title: "Good Crypto: one trading app for all exchanges"
altTitle: 
authors:

users: 50000
appId: app.goodcrypto
launchDate: 
latestUpdate: 2021-02-25
apkVersionName: "1.6.2"
stars: 4.5
ratings: 370
reviews: 163
size: 23M
website: https://goodcrypto.app
repository: 
issue: 
icon: app.goodcrypto.png
bugbounty: 
verdict: nowallet # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-08
reviewStale: true
signer: 
reviewArchive:


providerTwitter: GoodCryptoApp
providerLinkedIn: company/goodcrypto
providerFacebook: GoodCryptoApp
providerReddit: GoodCrypto

redirect_from:
  - /app.goodcrypto/
---


This app allows you to connect to accounts on trading platforms and does not
work as a wallet as such. We assume that you cannot receive Bitcoins to and send
them from this app.
