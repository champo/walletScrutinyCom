---
wsId: coholdmobile
title: "HOLD — Buy Bitcoin & Crypto"
altTitle: 
authors:
- leo
users: 10000
appId: co.hold.mobile
launchDate: 
latestUpdate: 2021-03-09
apkVersionName: "3.12.9"
stars: 4.4
ratings: 138
reviews: 81
size: Varies with device
website: https://hold.io
repository: 
issue: 
icon: co.hold.mobile.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-03-10
reviewStale: true
signer: 
reviewArchive:


providerTwitter: HoldHQ
providerLinkedIn: company/holdhq
providerFacebook: HoldHQ
providerReddit: 

redirect_from:

---


> SAFETY FIRST<br>
  Regulated and licensed in the EU. Your money is securely held by banks within
  the European Union and your crypto protected by the world-renowned custodian
  BitGo.

This app is a custodial offering and thus **not verifiable**.
