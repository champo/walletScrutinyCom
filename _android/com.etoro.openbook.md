---
wsId: 
title: "eToro - Smart crypto trading made easy"
altTitle: 
authors:
- leo
users: 5000000
appId: com.etoro.openbook
launchDate: 2013-11-05
latestUpdate: 2021-03-25
apkVersionName: "314.0.0"
stars: 2.8
ratings: 73424
reviews: 29323
size: 79M
website: 
repository: 
issue: 
icon: com.etoro.openbook.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: etoro
providerLinkedIn: company/etoro
providerFacebook: eToro
providerReddit: 

redirect_from:
  - /com.etoro.openbook/
  - /posts/com.etoro.openbook/
---


This page was created by a script from the **appId** "com.etoro.openbook" and public
information found
[here](https://play.google.com/store/apps/details?id=com.etoro.openbook).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.
