---
wsId: 
title: "Zeus: Bitcoin/Lightning Wallet"
altTitle: 
authors:
- leo
users: 500
appId: app.zeusln.zeus
launchDate: 
latestUpdate: 2021-03-26
apkVersionName: "0.5.1"
stars: 4.1
ratings: 15
reviews: 9
size: 55M
website: 
repository: 
issue: 
icon: app.zeusln.zeus.png
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-07-15
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /app.zeusln.zeus/
  - /posts/app.zeusln.zeus/
---


