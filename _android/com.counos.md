---
wsId: counos
title: "Counos Bitcoin Wallet"
altTitle: 
authors:
- leo
users: 10000
appId: com.counos
launchDate: 
latestUpdate: 2021-03-02
apkVersionName: "1.9.46"
stars: 4.1
ratings: 510
reviews: 350
size: 38M
website: https://www.counos.io
repository: 
issue: 
icon: com.counos.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-05
reviewStale: true
signer: 
reviewArchive:


providerTwitter: counoscoin
providerLinkedIn: company/11734790
providerFacebook: CounosPlatform
providerReddit: 

redirect_from:
  - /com.counos/
---


Nothing in the description hints at this app being non-custodial.

On the website the section about the Android wallet reads:

> In addition to creating a user account and a password, the users can use the biometric parameters such as fingerprint sensor and face recognition sensor for the two-stage entrance to the wallet. Counos wallet is able to support a wide range of valid global cryptocurrencies from Counos Coin, and Counos Cash to Bitcoin and Ethereum. Through creating a unique address for the wallet and also generating QR codes, the users can comfortably do all their transfers via the Counos Android wallet.

which certainly is no statement towards the wallet being non-custodial. As a
custodial wallet it is **not verifiable**.
