---
wsId: 
title: "GoldWallet - Bitcoin Vault Wallet"
altTitle: 
authors:

users: 10000
appId: io.goldwallet.wallet
launchDate: 
latestUpdate: 2020-11-26
apkVersionName: "6.0.1"
stars: 4.3
ratings: 439
reviews: 249
size: 35M
website: https://bitcoinvault.global
repository: 
issue: 
icon: io.goldwallet.wallet.png
bugbounty: 
verdict: nobtc # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-14
reviewStale: false
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /io.goldwallet.wallet/
---


This app appears to not be a vault for Bitcoin but something for Bitcoin Vault.
