---
wsId: bithumbko
title: "빗썸 트레이더"
altTitle: 
authors:
- leo
users: 50000
appId: com.btckorea.bithumba
launchDate: 
latestUpdate: 2021-01-30
apkVersionName: "1.1.0"
stars: 2.2
ratings: 382
reviews: 216
size: 19M
website: https://www.bithumb.com
repository: 
issue: 
icon: com.btckorea.bithumba.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-02-19
reviewStale: false
signer: 
reviewArchive:


providerTwitter: BithumbOfficial
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This app appears to be the korean version of
[this app](/android/com.btckorea.bithumb/). Google Translate doesn't reveal any
substantial difference. We conclude it is a custodial offering and **not verifiable**.
