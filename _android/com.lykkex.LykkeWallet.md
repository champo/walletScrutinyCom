---
wsId: lykkex
title: "Lykke: Trade, Buy & Store Bitcoin, Crypto and More"
altTitle: 
authors:
- leo
users: 50000
appId: com.lykkex.LykkeWallet
launchDate: 
latestUpdate: 2020-04-07
apkVersionName: "12.7.7"
stars: 3.5
ratings: 566
reviews: 344
size: 23M
website: https://lykke.com/wallet
repository: 
issue: 
icon: com.lykkex.LykkeWallet.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-03-09
reviewStale: true
signer: 
reviewArchive:


providerTwitter: Lykke
providerLinkedIn: company/lykke
providerFacebook: LykkeCity
providerReddit: lykke

redirect_from:

---


Lykke appears to be a Swiss exchange:

> Lykke is your gateway to the future of investment, allowing you to securely
  buy, sell and store Bitcoin & other cryptocurrencies on the fully regulated
  Swiss-based Lykke Exchange & wallet.

but there is no claim about the wallet being self-custodial or even hints like
references to industry standards in self-custodial wallets.

We only see

> - Store your Crypto-Assets safely in our secure Blockchain private wallet

but that could mean anything. For now we assume this app is the interface to an
account on the custodial exchange of the same name which makes it
**not verifiable**.
