---
wsId: 
title: "RenrenBit"
altTitle: 
authors:
- leo
users: 500
appId: com.dcash.wallet
launchDate: 
latestUpdate: 2020-07-27
apkVersionName: "1.7.6"
stars: 5.0
ratings: 5
reviews: 2
size: 33M
website: 
repository: 
issue: 
icon: com.dcash.wallet.png
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-03-04
reviewStale: false
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This app looks like a custodial offering:

> RenrenBit is a digital asset management platform offering 100% Reserve.

As it only has 100 downloads its category is "few users" though.

Suspiciously Google Play features no website, the contact is
`muhuai@renrenbit.com` but renrenbit.com featuring both Android and iPhone logos
for app downloads that only appear to link to an
[app on iPhone](https://apps.apple.com/us/app/renrenbit/id1443447248).
