---
wsId: TECHBANK
title: "TechBank"
altTitle: 
authors:
- leo
users: 10000
appId: com.beeone.techbank
launchDate: 
latestUpdate: 2021-03-29
apkVersionName: "4.4"
stars: 4.3
ratings: 500
reviews: 207
size: 41M
website: https://techbank.finance
repository: 
issue: 
icon: com.beeone.techbank.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-14
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.beeone.techbank/
---


Neither on Google Play nor their website do we found claims about this app being
a non-custodial wallet and as the name Tech**Bank** sounds rather custodial, we
file it as such and conclude this app is **not verifiable**.
