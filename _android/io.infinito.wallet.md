---
wsId: InfinitoWallet
title: "Infinito Wallet - Crypto Wallet & DApp Browser"
altTitle: 
authors:
- leo
users: 100000
appId: io.infinito.wallet
launchDate: 2017-11-15
latestUpdate: 2021-03-18
apkVersionName: "2.35.1"
stars: 3.5
ratings: 2089
reviews: 986
size: 64M
website: https://www.infinitowallet.io
repository: 
issue: 
icon: io.infinito.wallet.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-03-30
reviewStale: true
signer: 
reviewArchive:


providerTwitter: Infinito_Ltd
providerLinkedIn: company/infinitoservices/
providerFacebook: InfinitoWallet
providerReddit: 

redirect_from:
  - /io.infinito.wallet/
  - /posts/io.infinito.wallet/
---


Right on the Playstore description we find:

> Truly decentralized: You control private key, passphrases and passwords

So it is not a custodial app. How about source code?

Searching for its appId on GitHub
[does not yield promising results](https://github.com/search?q=%22io.infinito.wallet%22&type=Code)
neither.

Our verdict: This app is **not verifiable**.
