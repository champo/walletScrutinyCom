---
wsId: 
title: "Coini — Bitcoin / Cryptocurrencies"
altTitle: 
authors:

users: 5000
appId: partl.coini
launchDate: 
latestUpdate: 2021-03-23
apkVersionName: "2.1.2"
stars: 4.6
ratings: 178
reviews: 90
size: 46M
website: 
repository: 
issue: 
icon: partl.coini.png
bugbounty: 
verdict: nowallet # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-14
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /partl.coini/
---


This app is for portfolio tracking but probably is not in control of private keys.
