---
wsId: LiteBit
title: "LiteBit - Buy & sell Bitcoin"
altTitle: 
authors:
- leo
users: 50000
appId: com.flutter.litebit
launchDate: 
latestUpdate: 2021-03-18
apkVersionName: "3.0.3"
stars: 2.8
ratings: 486
reviews: 363
size: 82M
website: https://www.litebit.eu
repository: 
issue: 
icon: com.flutter.litebit.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-06
reviewStale: true
signer: 
reviewArchive:


providerTwitter: litebiteu
providerLinkedIn: company/litebit
providerFacebook: litebiteu
providerReddit: 

redirect_from:
  - /com.flutter.litebit/
---


> All you need is a LiteBit account.

If you need an account, it's probably custodial.

On their website there is no contrary claims so we assume this app is
**not verifiable**.
