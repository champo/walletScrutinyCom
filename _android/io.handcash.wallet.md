---
wsId: 
title: "HandCash"
altTitle: 
authors:

users: 10000
appId: io.handcash.wallet
launchDate: 2019-09-10
latestUpdate: 2021-03-19
apkVersionName: "2.5.11"
stars: 4.4
ratings: 392
reviews: 257
size: 33M
website: https://handcash.io
repository: 
issue: 
icon: io.handcash.wallet.png
bugbounty: 
verdict: nobtc # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2019-12-28
reviewStale: true
signer: 
reviewArchive:


providerTwitter: handcashapp
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /io.handcash.wallet/
  - /posts/io.handcash.wallet/
---


A BSV wallet.
