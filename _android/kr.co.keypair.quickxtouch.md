---
wsId: krcokeypair
title: "QuickX Touch"
altTitle: 
authors:

users: 500
appId: kr.co.keypair.quickxtouch
launchDate: 
latestUpdate: 2019-07-17
apkVersionName: "1.0.0.57"
stars: 3.7
ratings: 14
reviews: 10
size: 10M
website: https://www.quickx.io
repository: 
issue: 
icon: kr.co.keypair.quickxtouch.jpg
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-03-07
reviewStale: true
signer: 
reviewArchive:


providerTwitter: quickxprotocol
providerLinkedIn: company/quickx
providerFacebook: quickxprotocol
providerReddit: QuickX

redirect_from:

---


<!-- nosource -->
As far as we can see, this is the same as
[this app](/android/kr.co.keypair.keywalletTouch) and thus is **not verifiable**.
