---
wsId: 
title: "Electron Cash wallet for Bitcoin Cash"
altTitle: 
authors:

users: 10000
appId: org.electroncash.wallet
launchDate: 2018-12-08
latestUpdate: 2021-03-02
apkVersionName: "4.2.3-3"
stars: 4.1
ratings: 141
reviews: 71
size: 35M
website: https://electroncash.org
repository: 
issue: 
icon: org.electroncash.wallet.png
bugbounty: 
verdict: nobtc # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2019-12-20
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /org.electroncash.wallet/
  - /posts/org.electroncash.wallet/
---


