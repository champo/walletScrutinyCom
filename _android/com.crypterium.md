---
wsId: crypterium
title: "Crypterium | Bitcoin Wallet"
altTitle: 
authors:
- leo
users: 100000
appId: com.crypterium
launchDate: 
latestUpdate: 2021-03-18
apkVersionName: "2.6.42.8"
stars: 3.8
ratings: 6437
reviews: 3458
size: 46M
website: https://crypterium.com
repository: 
issue: 
icon: com.crypterium.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-01
reviewStale: true
signer: 
reviewArchive:


providerTwitter: crypterium
providerLinkedIn: 
providerFacebook: crypterium.org
providerReddit: 

redirect_from:
  - /com.crypterium/
---


This app is a custodial offering with many many users complaining about never
having been able to get their funds out. The app is **not verifiable**.
